import { Component, EventEmitter, Output, OnInit } from '@angular/core';

import { MachineState } from '../machine-state';

@Component({
  selector: 'app-code-input',
  templateUrl: './code-input.component.html',
  styleUrls: ['./code-input.component.css']
})
export class CodeInputComponent implements OnInit {
  msg: string;
  msgErr: boolean = false;
  in: string;
  machineState: MachineState;
  @Output() compiled = new EventEmitter<MachineState>();

  run(): void {
    this.msgErr = false;
    this.msg = 'Compiling…';
    try {
      this.compile();
    } catch(e) {
      this.msg = e;
      this.msgErr = true;
      return;
    }
    this.compiled.emit(this.machineState);
    this.msg = 'Compilation finished!';
  }

  compile(): void {
    if (this.in == null) { throw 'Type something in the text box!'; }
    const lines: string[] = this.in.split("\n");
    for (const line of lines) {
      const tokens: string[] = line.split(" ");
      const registers: string[] = line.match(/\br[0-9]{1,2}\b/g);
      if (registers == null) { throw 'Invalid program'; }
      switch(tokens[0]) {
        case 'addi':
          if(registers.length == 2) {
            this.machineState.addi(
              parseInt(registers[0].substring(1)),
              parseInt(registers[1].substring(1)),
              parseInt(tokens[3])
            );
          } else { throw 'Not enough register arguments!'; } break;
        default: throw 'Unknown opcode';
      }
    }
  }

  constructor() {}


  ngOnInit() {
    this.machineState = new MachineState();
  }

}
