import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CodeInputComponent } from './code-input/code-input.component';
import { CodeOutputComponent } from './code-output/code-output.component';

@NgModule({
  declarations: [
    AppComponent,
    CodeInputComponent,
    CodeOutputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
