export class MachineState {
  r: number[];
  private pc: number;

  addi(dest: number, src: number, imm: number): void {
    this.r[dest] = this.r[src] + imm;
    this.pc++;
  }

  getPc(): number { return this.pc }

  constructor() {
    this.r = new Array<number>(32);
    for(let i = 0; i < this.r.length; i++) {
      this.r[i] = 0;
    }
    this.pc = 0;
  }
}
