import { Component, OnInit, Input } from '@angular/core';

import {MachineState} from '../machine-state';

@Component({
  selector: 'app-code-output',
  templateUrl: './code-output.component.html',
  styleUrls: ['./code-output.component.css']
})
export class CodeOutputComponent implements OnInit {
  state: MachineState;

  setState(newState: MachineState): void {
    this.state = newState;
  }

  constructor() { }

  ngOnInit() {
    this.state = new MachineState();
  }

}
